process.env.PORT = 3000;
process.env.ENV = "dev";
process.env.DB_USER = "fake-user";
process.env.DB_PASSWORD = "fake-password";
process.env.DB_NAME = "fake-db-name";
process.env.DB_PATH = "fake-path";