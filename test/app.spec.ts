import expect from 'expect';
import sinon, {SinonStub} from 'sinon';
import request from 'supertest';
import mongoose from 'mongoose';
import {OK, INTERNAL_SERVER_ERROR, BAD_REQUEST} from 'http-status-codes';

import App from '../src/app';
import PersonController from '../src/controllers/Person';

import { Person, personDb } from '../src/models/Person';

describe('App Class', () => {
    let mongoConnect: SinonStub;
    let app: App;
  
    const error = new Error('Fake Error');
  
    beforeEach(() => {
      mongoConnect = sinon.stub(mongoose, 'connect' as any);
  
      app = new App([
        new PersonController()
      ]);
    });
  
    afterEach(() => {
      if (mongoConnect && mongoConnect.restore) mongoConnect.restore();
    });
  
    it('Should instantiate App', () => {
      mongoConnect.returns({});
  
      expect(app).toBeDefined();
      expect(mongoConnect.calledOnce).toBeTruthy();
    });
  
    describe('Person', () => {
      let findPersons: SinonStub;
  
      const persons: Array<Person> = [
        {
            name: 'fake-name-1',
            age: 25
        },
        {
            name: 'fake-name-1',
            age: 25
        }
      ];
  
      beforeEach(() => {
        findPersons = sinon.stub(personDb, 'find' as any);
      });
  
      afterEach(() => {
        if (findPersons && findPersons.restore) findPersons.restore();
      });
  
      it('Should get a list of persons', done => {
        findPersons.returns(persons);
  
        request(app.getServer())
          .get('/api/person/all')
          .expect('Content-Type', 'application/json')
          .expect(OK)
          .end((err, res) => {
            if (err) {
              done(err)
            } else {
              const {body} = res;
              expect(body).toEqual(persons);
              expect(findPersons.calledOnce).toBeTruthy();
  
              done();
            }
          });
      });
  
      it('Should get 500 if get all throws an error', done => {
        findPersons.throws(error);
  
        request(app.getServer())
        .get('/api/person/all')
          .expect('Content-Type', 'application/json')
          .expect(INTERNAL_SERVER_ERROR)
          .end((err, res) => {
            if (err) {
              done(err)
            } else {
              const {body} = res;
              expect(body).toEqual(error.message);
              expect(findPersons.calledOnce).toBeTruthy();
  
              done();
            }
          });
      });

      it('This test should fail', done => {
        expect(false).toBeTruthy();
      })
    });
  });