import express, { Application } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
import Controller from './models/Controller';

export default class App {
  private readonly app: Application;

  public constructor(controllers: Controller[]) {
    this.app = express();

    App.connectToDatabase();
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
  }

  public getServer(): Application {
    return this.app;
  }

  public listen(): void {
    this.app.listen(process.env.PORT, (): void => {
      console.log(`App listening on the port ${process.env.PORT}`);
    });
  }

  private static connectToDatabase(): void {
    const { DB_USER, DB_PASSWORD, DB_PATH, DB_NAME } = process.env;
    mongoose.connect(
      `mongodb+srv://${DB_USER}:${DB_PASSWORD}${DB_PATH}/${DB_NAME}?retryWrites=true`,
      {
        useNewUrlParser: true
      }
    );
  }

  private initializeMiddlewares(): void {
    this.app.use(cors());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
  }

  private initializeControllers(controllers: Controller[]): void {
    controllers.forEach((controller): void => {
      this.app.use('/', controller.router);
    });
  }
}
