import { Router, Request, Response } from 'express';
import { personDb } from '../models/Person';
import Controller from '../models/Controller';
import respond from '../utils/response';
import { INTERNAL_SERVER_ERROR, OK } from 'http-status-codes';

export default class PersonController implements Controller {
  public path = '/api/person';
  public router = Router();
  private person = personDb;

  public constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes(): void {
    this.router.get(`${this.path}/all`, this.getAll);
  }

  private getAll = async (request: Request, response: Response): Promise<void> => {
    try {
      const eventos = await this.person.find();

      respond(response, OK, eventos);
    } catch (e) {
      console.log('INTERNAL_SERVER_ERROR', e.message);
      respond(response, INTERNAL_SERVER_ERROR, e.message);
    }
  };
}
