import App from './app';
import validateEnv from './utils/validateEnv';
import PersonController from './controllers/Person';

validateEnv();

const app = new App([new PersonController()]);

app.listen();
