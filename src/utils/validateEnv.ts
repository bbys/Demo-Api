import { cleanEnv, port, str } from 'envalid';

export default function ValidateEnv(): void {
  cleanEnv(process.env, {
    PORT: port(),
    ENV: str(),
    DB_USER: str(),
    DB_PASSWORD: str(),
    DB_NAME: str(),
    DB_PATH: str()
  });
}
