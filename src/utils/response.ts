import { Response } from 'express';

// eslint-disable-next-line
export default (res: Response, status: number, data: any, contentType: string = 'application/json') => {
  res.writeHead(status, {
    'Content-Type': contentType
  });
  res.end(JSON.stringify(data));
};
