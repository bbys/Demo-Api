import mongoose, { Schema, Document } from 'mongoose';

export interface Person {
  name: string;
  age: number;
}

const PersonSchema: Schema = new Schema({
  name: {
    type: String,
    required: true
  },
  age: {
    type: Number,
    required: false
  }
});

export const personDb = mongoose.model<Person & Document>('Person', PersonSchema);
